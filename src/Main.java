import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        System.out.print("Enter First Name: ");
        firstName = scan.nextLine();
        System.out.print("Enter Last Name: ");
        lastName = scan.nextLine();
        System.out.print("Enter First Subject: ");
        firstSubject = scan.nextDouble();
        System.out.print("Enter Second Subject: ");
        secondSubject = scan.nextDouble();
        System.out.print("Enter Third Subject: ");
        thirdSubject = scan.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("\nFirst Name:");
        System.out.println(firstName);
        System.out.println("Last Name:");
        System.out.println(lastName);
        System.out.println("First Subject Grade:");
        System.out.println(firstSubject);
        System.out.println("Second Subject Grade:");
        System.out.println(secondSubject);
        System.out.println("Third Subject Grade:");
        System.out.println(thirdSubject);

        System.out.println("Good day, " + firstName + lastName);
        System.out.println("Your grade average is: " + Math.round(average));
    }
}